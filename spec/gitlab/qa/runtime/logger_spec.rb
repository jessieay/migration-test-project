# frozen_string_literal: true

RSpec.describe Gitlab::QA::Runtime::Logger do
  let(:log_path) { '/tmp/qa-log' }

  before do
    described_class.instance_variable_set(:@logger, nil)

    allow(Gitlab::QA::TestLogger).to receive(:logger).and_return(nil)

    allow(Gitlab::QA::Runtime::Env).to receive(:log_path).and_return(log_path)
    allow(File).to receive(:exist?).and_return(file_exist)
    allow(FileUtils).to receive(:mkdir_p).with(log_path)
  end

  context 'with existing log path' do
    let(:file_exist) { true }

    it 'does not create log path and returns logger' do
      described_class.logger

      expect(Gitlab::QA::TestLogger).to have_received(:logger).with(level: 'INFO', path: log_path)
      expect(FileUtils).not_to have_received(:mkdir_p)
    end
  end

  context 'without existing log path' do
    let(:file_exist) { false }

    it 'creates log path and returns logger' do
      described_class.logger

      expect(Gitlab::QA::TestLogger).to have_received(:logger).with(level: 'INFO', path: log_path)
      expect(FileUtils).to have_received(:mkdir_p)
    end
  end
end
